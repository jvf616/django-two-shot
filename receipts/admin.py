from django.contrib import admin
from .models import ExpenseCategory, Receipt, Account

# Register your models here.


class ReceiptAdmin(admin.ModelAdmin):
    pass


admin.site.register(ExpenseCategory)
admin.site.register(Receipt, ReceiptAdmin)
admin.site.register(Account)
