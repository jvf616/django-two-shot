from django.shortcuts import render
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login
from django.shortcuts import redirect

# Create your views here.

# what I thought it could be
# class UserCreationForm:
#     def sign_up(request):
#         username = request.POST["username"]
#         password = request.POST["password"]
#         user = User.objects.create_user(
#             request, username=username, password=password
#         )
#         if user is not None:
#             login(request, user)
#             return redirect("home")
#         else:
#             return "invalid login"

# user = User.objects.create_user()


def sign_up(request):
    if request.method == "POST":
        form = UserCreationForm(request.POST)
        if form.is_valid():
            username = request.POST.get("username")
            password = request.POST.get("password1")
            user = User.objects.create_user(
                username=username, password=password
            )
            user.save()
            login(request, user)
            return redirect("home")
    else:
        form = UserCreationForm()
    context = {"form": form}
    return render(request, "registration/signup.html", context)
